# Formations en ligne

- [FuturCoder](https://fr.futurecoder.io/) Tutoriel de programmation interactif pour apprendre à programmer en Python

- [France IOI](http://www.france-ioi.org/algo/index.php)  Site d'entraînement permetant de progresser rapidement en programmation et en algorithmique, par un apprentissage allant de la découverte des bases de la programmation jusqu'au niveau des compétitions internationales les plus prestigieuses, en Python, C, C++, JavaScool, Java, OCaml ou Pascal.


- [Code](https://studio.code.org/courses?lang=fr-FR) Cours d'apprentissage de l'informatique pour les élèves (Anglais).

- [PIX](https://pix.fr/enseignement-scolaire/) Développer et certifier ses compétences numériques.