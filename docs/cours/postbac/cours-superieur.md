# Des liens vers des cours de l'enseignement supérieur

???note "Cours du DIU du Havre"
    === "Les cours"
        [DIU du Havre](https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/)
    === "Une vidéo"
        [video du Bloc 2](https://www.youtube.com/watch?v=Z-Wrpy1yL0A)

???note "Cours de Bruno Mermet"
    === "Cours sur les tests P1"
        [Cours 1](https://mermet.users.greyc.fr/Formations/FormationTestsMai2021/)
    === "Cours sur les tests P2"
        [Cours 2](https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/Algorithmique/tests.html)
    === "Cours sur le Min/max"
        [Liens](https://mermet.users.greyc.fr/Formations/FormationMinMaxAvril2021/)
    === "Une vidéo"
        [video min/max](https://www.youtube.com/watch?v=3LdhPtv87k0)

[DIU de Bordeaux : Programmation avancée et base de données](https://diu-uf-bordeaux.github.io/bloc4/)

[Cours sur les fondements de l'informatique (Polytechnique)](https://www.enseignement.polytechnique.fr/informatique/INF412/i.php?n=Main.Poly)


[Repository cours MP2I](https://github.com/mp2i-fsm/mp2i-2021)

[Capes de NSI](https://capes-nsi.org/ressources/#session-2022)

    
    
[Correction Capes de NSI (Irem Réunion)](https://irem.univ-reunion.fr/spip.php?rubrique169)


  
             