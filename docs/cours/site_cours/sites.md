# Quelques sites de professeurs de NSI

???note "Le site de Vincent Bouillot"
    === "Lien 1NSI"
        [Première NSI](https://ferney-nsi.gitlab.io/premiere/)
    === "Lien TNSI"
        [Terminale NSI](https://ferney-nsi.gitlab.io/terminale/)

???note "Le site de Franck Chambon"
    === "Lien 1NSI"
        [Première NSI](https://ens-fr.gitlab.io/nsi1/)
    === "Lien TNSI"
        [Terminale NSI](https://ens-fr.gitlab.io/nsi2/)


???note "Le site de Romain Janvier"
    [site de TNSI](http://nsiterminale.janviercommelemois.fr/)

???note "Le site de Vincent-Xavier Jumel"
    === "Lien 1NSI"
        [Première NSI](https://lamadone.frama.io/informatique/premiere-nsi/index.html)
    === "Lien TNSI"
        [Terminale NSI](https://lamadone.frama.io/informatique/terminale-nsi/index.html)

???note "Le site de Gilles Lassus"
    === "Lien 1NSI"
        [Première NSI](https://glassus.github.io/premiere_nsi/)
    === "Lien TNSI"
        [Terminale NSI](https://glassus.github.io/terminale_nsi/)



???note "Le site d'Olivier Lecluse"
    === "Lien TNSI"
        [Terminale NSI](https://www.lecluse.fr/nsi/NSI_T/)
    === "Notebooks"
        [notebooks](https://notebooks.lecluse.fr/)




???note "Le site de Fred Leleu"
    [site NSI](https://impytoyable.fr/index.html)

???note "Le site de Frédéric Mandon"
    === "Lien 1NSI"
        [Première NSI](http://www.maths-info-lycee.fr/nsi_1ere.html)
    === "Lien TNSI"
        [Terminale NSI](http://www.maths-info-lycee.fr/nsi.html)    

???note "Le site de Fabrice Nativel"
    === "Lien 1NSI"
        [Première NSI](https://fabricenativel.github.io/NSIPremiere/)
    === "Lien TNSI"
        [Terminale NSI](https://fabricenativel.github.io/NSITerminale/)


???note "Le site Rodrigo Schwencke"
    === "Lien 1NSI"
        [Première NSI](https://eskool.gitlab.io/1nsi/)
    === "Lien TNSI"
        [Terminale NSI](https://eskool.gitlab.io/tnsi/)

???note "Le site de  Stéphan Van Zuijlen"
    === "Lien 1NSI"
        [Première NSI](https://isn-icn-ljm.pagesperso-orange.fr/1-NSI/index.html)
    === "Lien TNSI"
        [Terminale NSI](https://isn-icn-ljm.pagesperso-orange.fr/NSI-TLE/index.html)   

???note "Le site des pixees"
    === "Lien 1NSI"
        [Première NSI](https://pixees.fr/informatiquelycee/n_site/nsi_prem.html)
    === "Lien 1NSI bis"
        [Première NSI](https://pixees.fr/informatiquelycee/prem/index.html)
    === "Lien TNSI"
        [Terminale NSI](https://pixees.fr/informatiquelycee/n_site/nsi_term.html)
    === "Lien TNSI bis"
        [Terminale NSI](https://dav74.github.io/site_nsi_term/)



???note "Le site Proalgo"
    [site NSI](https://progalgo.fr/)

???note "Le site Mon lycée numérique"
    [site NSI](http://www.monlyceenumerique.fr/index_nsi.html)




???note "Le site du lycée Angellier"
    === "Lien 1NSI"
        [Première NSI](https://angellier.gitlab.io/nsi/premiere/)
    === "Lien TNSI"
        [Terminale NSI](https://angellier.gitlab.io/nsi/terminale/)



???note "Le site du lycée Louis de Foix de Bayonne"
    [site de TNSI](http://tnsi.free.fr/)

???note "Le site du lycée Toulouse centre"
    [site NSI](https://sites.google.com/view/nsi-toulouse-centre/accueil)



???note "Le site Maths-code"
    === "Lien 1NSI"
        [Première NSI](http://maths-code.fr/cours/premiere-nsi-2/)
    === "Lien TNSI"
        [Terminale NSI](http://maths-code.fr/cours/terminale-nsi/)

???note "Projet eSkool"
    [Repository](https://gitlab.com/eskool/tnsi/-/tree/main)


???note "Notebooks de Eric Morlaix"
    [1NSI](https://nbviewer.org/github/ericECmorlaix/1NSI_2019-2020/tree/master/)

???note "Site du lycée Langevin"
    [STI2D SIN](http://tsin.langevin-la-seyne.fr/SIN/)