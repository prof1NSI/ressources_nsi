# Sites divers pour illustrer le cours


- [Programmes de la spécialité NSI](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g) 

- [Eduscol NSI](https://nsi-snt.ac-normandie.fr/ressources-d-accompagnement-eduscol-pour-nsi) Ressources d'accompagnement Eduscol pour la NSI.


- [Lumni 1NSI](https://www.lumni.fr/lycee/premiere/voie-generale)  et [Lumni TNSI](https://www.lumni.fr/lycee/terminale/voie-generale) Contenus pédagogiques pour préparer les cours avec Lumni.


- [Site Basthon](https://basthon.fr/) Un bac à sable pour Python, SQL, Ocaml, JS, dans le navigateur !

- [Genumsi](https://genumsi.inria.fr/)
(Où l'on peut créer un QCM pour les cours de NSI)


- [Interpreteur Pyton](https://www.pythonanywhere.com/embedded3/)

- [Animation des algorithmes par Fred Boissac](http://fred.boissac.free.fr/AnimsJS/Dariush_Anims/index.html)

- [Articles](https://professeurb.github.io/articles/)
(Où l'on peut trouver un certain nombre d'articles traîtant d'aspects variés de l'informatique…)


- [Mermaid](https://mermaid.live/edit#pako:eNpV0E2LwyAQgOG_EmYvE0jAtvvR5rZp0v067h69SNRtwMRizaGU_vedmAqrp_HhBWWu0FmpoIJfJ07H7KfhY0bnFXGV51lZllmNuM7zOwfZI26i1EEaxMdUWsSnKPsgB8TnVN4QX6I0Qd4Rt6l8IO6itEE-6WsspS-i1UwLnv3FKHpD98ZUD5qxorPGOhq1_l_U94IxnRRzAwUMyg2il7Sb6ywc_FENikNFo1RaTMZz4OON0ukkhVet7L11UHk3qQLE5O33ZezifWmaXtCmhwVvf8nkZDk) Différents diagrammes que l'on peut éditer sous différents formats, où intégrer à d'autres documents.

- Démonstration des propriétés de Flexbox (CSS) [CodePen par Dimitar](https://codepen.io/justd/pen/yydezN)

- [AlgoGraphe](http://frederic.zinelli.gitlab.io/graph-application/) par Frederic Zinelli 
(une application dédiée à la création de graphes, leur affichage, leur conversion en structure de données, et l’animation d’algorithmes sur les-dits graphes...) [Readme](https://gitlab.com/frederic.zinelli/graph-application/-/blob/main/README.md) et [dossier à téléchargeable](https://gitlab.com/frederic.zinelli/graph-application/-/tree/main/dist/graph-v0.3.1)