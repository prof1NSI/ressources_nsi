# Sites de cours en ligne sur la programmation


- [FUN France Université Numérique](https://www.fun-mooc.fr/fr/) Formations informatiques diverses

- [OpenClassRoom](https://openclassrooms.com/fr/) Formations informatiques diverses


- [W3Schools](https://www.w3schools.com/) Cours sur différents langages (développement Web, Python, Java, C, C++, C#, Ruby...) + exemples

- [Resources for developers MDN](https://developer.mozilla.org/fr/) Aide pour le développement Web (HTML, CSS, Javascript) par Mozilla

- [Apprendre le langage Haskell](http://www.lyah.haskell.fr/chapitres) 

- [Codingame](https://www.codingame.com/start) Améliorez vos compétences en programmation.

- [Codewars](https://www.codewars.com/) Ameliorer ses compétences en codage en s'entrainant (Anglais).