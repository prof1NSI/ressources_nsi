# Modules Pythons

- Module **Flask** : un micro-framework  permettant de concevoir une application web [Site officiel](https://flask.palletsprojects.com/en/1.1.x/)

Tutoriels vidéo Flask : [Xavki](https://www.youtube.com/watch?v=vunWCJrwKx8) et [Paul Vincensini](https://www.youtube.com/watch?v=zDkUIKedFsU)

[Spectre](https://picturepan2.github.io/spectre/) A Lightweight, Responsive and Modern CSS Framework

[BootStrap](https://getbootstrap.com/) Build fast, responsive sites with Bootstrap

- Module **Pixel** :  Un moteur de jeu vidéo rétro pour Python.

[Pixel](https://nuitducode.github.io/DOCUMENTATION/PYTHON/01-presentation/)  et [Repository Pixel](https://github.com/kitao/pyxel/blob/main/doc/README.fr.md) 

- Module **Folium** : concevoir une carte affichée sur une page web [Site officiel](https://python-visualization.github.io/folium/)

[Labo de Maths](http://labodemaths.fr/WordPress3/nsi-tp10-decouverte-du-module-folium-et-tables-de-donnees/)  et [Site SIN ISN](http://sti2d-sin-isn.blogspot.com/2020/02/utilisation-basique-de-folium-sous.html)

- Module **Doctest** : un framework de test unitaire qui fait partie de la bibliothèque standard de Python [Documentation](https://docs.python.org/3/library/doctest.html)

[Fabien Loison : Flozz Blog](https://blog.flozz.fr/2020/06/15/doctest-vous-navez-aucune-excuse-pour-ne-pas-ecrire-des-tests-unitaires-en-python/) 

- Module **Pillow** : Travailler sur des images en Python [Documentation](https://pillow.readthedocs.io/en/stable/)


- **Pygame Zero** : un wrapper autour de pygame pour en simplifier l’utilisation

[Documentation de  Daniel Pope](https://pgzero-french.readthedocs.io/fr/latest/)

[Tutoriel de Yves Moncheaux](https://clogique.fr/nsi/premiere/tuto_pg0/tuto_pgzero.html#1)
