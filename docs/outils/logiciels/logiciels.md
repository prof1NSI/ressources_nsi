# Logiciels

- VS Codium :

[site officiel](https://vscodium.com/)

[Présentation Franck Chambon](https://franckchambon.github.io/ClasseVirtuelle/NSI/5-%C3%89diteurs/vscodium.html)


- Jupyter Notebook :

[site officiel](https://jupyter.org/)

[Raccourcis clavier](https://cheatography.com/weidadeyue/cheat-sheets/jupyter-notebook/)

[Colaboratory](https://colab.research.google.com/notebooks/welcome.ipynb)

- Filius : simulation d'un réseau informatique
 
[site officiel](https://www.lernsoftware-filius.de/Herunterladen)

[Guide Daniel Garmann](https://www.pearltrees.com/s/file/preview/205382473/Introduction%20Filius.pdf?pearlId=270715447)

[Ressources Académie de Bordeaux](https://ent2d.ac-bordeaux.fr/disciplines/sti-college/2019/09/25/filius-un-logiciel-de-simulation-de-reseau-simple-et-accessible/)

[Vidéo de David Roche](https://www.youtube.com/watch?v=K3GGmiLwB6U)