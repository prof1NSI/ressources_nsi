# Sites pratiques

- [Basthon](https://basthon.fr/) Bac à sable pour Python, SQL, Ocaml, JS et Python, dans le navigateur.

- [Pythontutor](https://pythontutor.com/visualize.html#mode=edit) Visualiser le code en Python, C, C++ et Java.

- [RealPython](https://realpython.com/) Tutoriels pour apprendre Python (Anglais).

- [Skult](http://skulpt.org/) implementation de Python dans une page web.

- [Brython](https://brython.info/index.html) Une implémentation de Python 3 pour la programmation web côté client

- [Fonts Google](https://fonts.google.com/)

- [Site Emoji](https://www.copyandpasteemoji.com/)

- [Lorem Ipsum](https://www.faux-texte.com/) Générateur de faux textes aléatoires.

- [Netlify](https://www.netlify.com/) Déployer son site en ligne.


- [Versionnage avec GIT](http://www.silanus.fr/nsi/premiere/git/git.html)


- [ObjGen](https://beta5.objgen.com/)  Live code generator pour JSON et HTML.


- [JSONLint](https://jsonlint.com/) Validateur de code JSON



