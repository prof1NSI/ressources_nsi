# Module Python Ursina

Interface graphique en 3D...

- [TP Ursacraft](https://clogique.fr/nsi/premiere/td_tp/TP_Ursacraft.html#1)

- [Documentation (Anglais)](https://www.ursinaengine.org/cheat_sheet.html)

- [Tutoriel PDF](https://www.ursinaengine.org/ursina_for_dummies.html)

- [Tutoriel Vidéo ](https://www.youtube.com/watch?v=w2gu9Ah95l0)

- [Tutoriel Vidéo Minecraft Like](https://www.youtube.com/watch?v=vX4l-qozib8)