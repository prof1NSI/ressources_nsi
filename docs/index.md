# Site Ressources NSI

Vous pourrez trouver ici des liens vers des sites conçus par des professeurs, des liens vers des applications en lignes, où à installer, en rapport avec des thèmes ou des notions du programme de NSI.

Un site fourre tout, où se trouve une partie des idées partagées par tous les collègues de NSI...



![NSI](/images/source.jpg)

![NSI](/images/logo-forum.png){width=2%}  [Vers le forum Numérique et Sciences Informatiques](https://mooc-forums.inria.fr/moocnsi/) 

![NSI](/images/eskool.jpg){width=3%}   [Vers le dépôt privé GitLab de documents partagés par les Enseignants de NSI](https://gitlab.com/eskool/profs-info/nsi-prive) 

