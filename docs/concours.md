# Concours Informatique

- [Castor](https://www.castor-informatique.fr/)

- [Algorea](https://algorea.org/#/)

- [La nuit du code](https://www.nuitducode.net/)

- [Prologin](https://prologin.org/)

- [Site du Concours général](https://www.education.gouv.fr/le-concours-general-des-lycees-et-des-metiers-un-prix-d-excellence-10022) et [Eduscol](https://eduscol.education.fr/1455/presentation-du-concours-general-des-lycees-et-des-metiers)

- [Girls Can Code](https://girlscancode.fr/)